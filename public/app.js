const SPADE = `<span class="symbol"><svg fill="black" viewBox="0 0 24 24" stroke-width="1.5" fill="none" xmlns="http://www.w3.org/2000/svg" color="#000000"><path d="M12 14.5c3 4.5 9 4.47 9-.5 0-4-4-7-9-12-5 5-9 8-9 12 0 4.97 6 5 9 .5z" stroke="#000000" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"></path><path d="M11.47 15.493l-3 5.625A.6.6 0 009 22h6a.6.6 0 00.53-.882l-3-5.625a.6.6 0 00-1.06 0z" stroke="#000000" stroke-width="1.5" stroke-linecap="round"></path></svg></span>`;

const cardContainer = document.getElementById("card-container");
const addCardButton = document.getElementById("add-card-button");

let rows = 0;

addCard();

cardContainer.addEventListener("submit", (e) => {
  e.preventDefault();
  window.print();
});

addCardButton.addEventListener("click", () => {
  addCard();
  cardContainer.querySelector("div:last-of-type .person input")?.focus();
  addCardButton.scrollIntoView();
});

function addCard() {
  const spadeIndex = Math.floor(Math.random() * 6);

  cardContainer.insertAdjacentHTML(
    "beforeend",
    `<div>
      <p class="person">
        <span class="symbol">P</span>
        <input type="text" name="person-${rows}" required />
        <span>${spadeIndex === 0 ? SPADE : ""}</span>
      </p>
      <p class="world">
        <span class="symbol">W</span>
        <input type="text" name="world-${rows}" required />
        <span>${spadeIndex === 1 ? SPADE : ""}</span>
      </p>
      <p class="object">
        <span class="symbol">O</span>
        <input type="text" name="object-${rows}" required />
        <span>${spadeIndex === 2 ? SPADE : ""}</span>
      </p>
      <p class="action">
        <span class="symbol">A</span>
        <input type="text" name="action-${rows}" required />
        <span>${spadeIndex === 3 ? SPADE : ""}</span>
      </p>
      <p class="nature">
        <span class="symbol">N</span>
        <input type="text" name="nature-${rows}" required />
        <span>${spadeIndex === 4 ? SPADE : ""}</span>
      </p>
      <p class="random">
        <span class="symbol">R</span>
        <input type="text" name="random-${rows}" required />
        <span>${spadeIndex === 5 ? SPADE : ""}</span>
      </p>
    </div>`
  );

  rows++;
}

// ripped off https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function shuffleArray(inputArray) {
  const array = Array.from(inputArray);

  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }

  return array;
}

console.log("Dedicated to Lois.");
